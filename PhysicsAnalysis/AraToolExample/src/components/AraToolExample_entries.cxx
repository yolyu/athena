#include "../AraToolExampleAlg.h"
#include "../AraToolUsingToolAlg.h"

#include "AraToolExample/AnExampleAraToolWrapper.h"
#include "AraToolExample/AraToolUsingToolWrapper.h"


DECLARE_COMPONENT( AnExampleAraToolWrapper )
DECLARE_COMPONENT( AraToolUsingToolWrapper )

DECLARE_COMPONENT( AraToolExampleAlg )
DECLARE_COMPONENT( AraToolUsingToolAlg )

